#!/bin/bash
#alias tm=tmux
#alias tma="tmux attach"
alias vialias="vim ~/aliases.sh && source ~/aliases.sh"
alias vm="ssh vm"

alias tmdev="tmux attach -t dev"
alias tmqa="tmux attach -t qa"
alias tmprod="tmux attach -t prod"
alias dev1="ssh dev1"
alias build2="ssh build2"
alias jbuild2="ssh jenkins@build2"

upload-ssh-key() {
  host=$1
  if [ -z "$host" ]; then
    echo "What host?"
    exit 1
  fi
  ssh $host 'mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys' < ~/.ssh/id_rsa.pub
}
