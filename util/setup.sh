#!/bin/bash

if [ ! -d "${MYCONFIG}" ]; then
  echo "\$MYCONFIG '${MYCONFIG}' is not a dir"
  return
fi

# export MYCONFIG_DEBUG=1

debug_myconfig() {
  if [ ! -z "$MYCONFIG_DEBUG" ]; then echo "MyConfig DEBUG ($MYCONFIG_BOX_NAME): $1"; fi
}

export MYCONFIG_COMMON="${MYCONFIG}/common"
export MYCONFIG_BOX_NAME="nonexistant"
export MYCONFIG_BOX="/nonexistant"

boxname_file=~/.boxname
if [ -f "${boxname_file}" ]; then
  export MYCONFIG_BOX_NAME=$(cat $boxname_file)
  export MYCONFIG_BOX="${MYCONFIG}/${MYCONFIG_BOX_NAME}-box"
else
  debug_myconfig "boxname file '$boxname_file' doesn't exist, not loading box name."
fi

debug_myconfig "MYCONFIG=$MYCONFIG"
debug_myconfig "MYCONFIG_COMMON=$MYCONFIG_COMMON"
debug_myconfig "MYCONFIG_BOX_NAME=$MYCONFIG_BOX_NAME"
debug_myconfig "MYCONFIG_BOX=$MYCONFIG_BOX"

#
# Source the "common" version of a config file.
# 
source_common_config() {
  path=$1
  common="${MYCONFIG_COMMON}/${path}"
  if [ -f "${common}" ]; then
    debug_myconfig "sourcing common config: ${specialized}"
    source "${common}"
  else
    echo "(can't find '${common}')"
  fi
}

#
# Source the "box specific" version of a config file.
#
source_my_config() {
  path=$1
  specialized="${MYCONFIG_BOX}/${path}"
  if [ -f "${specialized}" ]; then
    debug_myconfig "sourcing specialized config: ${specialized}"
    source "${specialized}"
  else
    echo "(can't find '${specialized}')"
  fi
}

#
# Attempt to load the box-specific version of a config file.
# Fallback by attempting to source the common version of the config file.
#
source_config() {
  path=$1
  specialized="${MYCONFIG_BOX}/${path}"
  common="${MYCONFIG_COMMON}/${path}"
  if [ -f "${specialized}" ]; then
    debug_myconfig "sourcing specialized config: ${specialized}"
    source "${specialized}"
  else
    if [ -f "${common}" ]; then
      debug_myconfig "sourcing common config: ${specialized}"
      source "${common}"
    else
      echo "(can't source '${specialized}' or '${common}')"
    fi
  fi
}

