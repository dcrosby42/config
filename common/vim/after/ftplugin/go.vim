"
" Go
" 
"let g:go_bin_path = $HOME."/tmp/gtmp/bin"

" format with goimports instead of gofmt
" let g:go_fmt_command = "goimports"

" disable fmt on save
" let g:go_fmt_autosave = 0

" if $VIMGO_OLDSTYLE == '1'
"   cabbr go_def_guru let g:go_def_mode = 'guru'
"   cabbr go_def_godef let g:go_def_mode = 'godef'
" else
"   let g:go_def_mode = 'gopls'
"   let g:go_info_mode = 'gopls'
" endif



" nnoremap <s-F5> :!go install tools/tellme/cmd/tellme<cr>
" noremap <F4>       :w<CR>:GoRun<CR>
" noremap <leader>gr :w<CR>:GoRun<CR>
" " dcrosby - I hacked the vim-go plugin for GoVet to include -composites=false for everything
" noremap <F5>       :w<CR>:GoVet<CR>
" noremap <leader>gv :w<CR>:GoVet<CR>
"
" map <F6> :w<CR>:!tclient "gk; echo DONE"<CR><CR>
" cabbr tt w<CR>:!tclient "gk; echo DONE"<CR><CR>
"
" noremap <F7> :GoDoc<CR>
" noremap <C-]> :GoDef<CR>
" noremap <C-\> :GoDefType<CR>
"
" map \d :GoDescribe<CR>
" map \i :GoImplements<CR>
" map \r :GoReferrers<CR>
"
" noremap <Leader>ln :s/Println/Printf<CR>
"
" noremap <Leader>gk :VimuxRunCommand "ginkgo"<cr>
" noremap <Leader>gkr :VimuxRunCommand "ginkgo -r"<cr>
"
" map <F8> :VimuxRunCommand "figlet OOOOOPS"<cr>
"
" map <leader>x I//XXX <esc>j0

" shortcut to edit this file:
cabbr govimrc e ~/.vim/after/ftplugin/go.vim
cabbr gosnip e ~/.vim/bundle/vim-snippets/snippets/go.snippets

" cabbr sum !$HOME/bin/ginkgo-to-text.sh %
"
" au FileType go nmap <Leader>ds <Plug>(go-def-split)
" au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
" au FileType go nmap <Leader>dt <Plug>(go-def-tab)
" au FileType go nmap <Leader>gr <Plug>(go-referrers)
