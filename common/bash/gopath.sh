#!/bin/bash

export_gopath_env() {
  go_path="${HOME}/.gopath"
  current_gopath_file="${go_path}/current"
  gp=$(cat $current_gopath_file)

  if [ "$gp" == "mod" ]; then
    unset VIMGO_OLDSTYLE
    unset GOPATH
    export PATH=$HOME/go/bin:$PATH
    unset gp
    return
  fi

  if [ -f "${current_gopath_file}" ]; then
    export GOPATH=$gp
    export PATH=$GOPATH/bin:$PATH
    # export VIMGO_OLDSTYLE=1
  else
    unset GOPATH
    echo "No GOPATH; missing file $current_gopath_file"
  fi
  unset gp
}

ensure_gopath_dir() {
  go_path="${HOME}/.gopath"
  choices_dir="${go_path}/choices"
  choices_dir="${go_path}/choices"

  [ -d "${go_path}" ] || mkdir -p "${go_path}"
  [ -d "${choices_dir}" ] || mkdir -p "${choices_dir}"
}

select_gopath() {
  nick=$1

  ensure_gopath_dir

  if [ $# -lt 1 ]; then
    echo "Usage : select_gopath <workspace_nickname>"
    echo "Available choices:"
    echo $(ls $choices_dir)
    return
  fi

  current_gopath_file="${go_path}/current"

  if [ "$nick" == "mod" ]; then
    echo "mod" > $current_gopath_file
    echo Using modules instead of gopath
    export_gopath_env
    echo "HEY! You may need to restart your shell to get a clean path. The old \$GOPATH/bin might still be in there."
    return
  fi

  choice="$choices_dir/$nick"
  if [ -f "$choice" ]
  then
    cp "$choice" "$current_gopath_file"
    echo Updated: "$current_gopath_file" "=>" $(cat $current_gopath_file)
    export_gopath_env
  else
    echo "FAIL: '$nick' isn't a valid choice."
  fi
}

add_gopath() {
  nick=$1
  path=$2

  ensure_gopath_dir
  
  echo "$path" > "$choices_dir/$nick"
  select_gopath $nick
}

alias gopath='echo $GOPATH'

# Bring gvm into play
if [[ -s ~/.gvm/scripts/gvm ]]; then
  source ~/.gvm/scripts/gvm
fi

# Make sure this happens AFTER gvm sourcing so we can manhandle our own gopath
export_gopath_env
