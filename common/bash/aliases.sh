#!/bin/bash

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grepnc='grep --color=never'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -ltha'

alias less='less -R'

alias tmc="tmux -CC"
alias tma="tmux -CC attach"

alias rmturds='find . -name ";" -exec rm {} '"';'"

alias pg="ping google.com"

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'


alias nocolor='sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"'
#
# crosby:
#
alias t=tree

alias z=z.sh

alias pf="ps aux | grep -v grep | grep"
alias hf="history | grep"


alias vialias="vim $MYCONFIG_COMMON/bash/aliases.sh && source $MYCONFIG_COMMON/bash/aliases.sh"
alias vimyalias="vim $MYCONFIG_BOX/bash/aliases.sh && source $MYCONFIG_BOX/bash/aliases.sh"
alias viprofile="vim ~/.bashrc && source ~/.bashrc"
alias viconfig="vim $MYCONFIG_COMMON"
alias cdconfig="cd $MYCONFIG_COMMON"
alias vimyconfig="vim $MYCONFIG_BOX"
alias cdmyconfig="cd $MYCONFIG_BOX"
alias configslam="$MYCONFIG/gitslam.sh"

#alias vij="vim ~/.local/share/autojump/autojump.txt"
alias vissh='vim ~/.ssh/config'
alias viknown='vim ~/.ssh/known_hosts'
alias vihosts='sudo vim /etc/hosts'

alias h=history

alias grenade='kill -9 %1'
alias grenade2='kill -9 %2'
alias grenade3='kill -9 %3'

alias murder='sudo kill -9'


alias mktags='ctags-exuberant -R .'

alias gk="figlet GINKGOOOOOO; echo; echo; echo; time ginkgo"
alias ngk="figlet GINKGOOOOOO; echo; echo; echo; nukeit; time ginkgo"
alias gkr="figlet GINKGOOOOOO; echo; echo; echo; time ginkgo -r"
# alias gkr="clear; time ginkgo -r"
alias gobr="go build -i ./..."


# Awk convenience
alias first="awk '{ print \$1 }'"
alias second="awk '{ print \$2 }'"
alias third="awk '{ print \$3 }'"
alias fourth="awk '{ print \$4 }'"
alias fifth="awk '{ print \$5 }'"
alias sixth="awk '{ print \$6 }'"
alias seventh="awk '{ print \$7 }'"
alias eighth="awk '{ print \$8 }'"
alias ninth="awk '{ print \$9 }'"
alias tenth="awk '{ print \$10 }'"


# GIT
alias gt="git status"
alias gad="git add"
alias gadl="git add . -A ; git st"
alias grabble="git add . -A ; git st"
alias gci="git commit -v"
alias push_to_dev="echo TO FORCE-PUSH A BRANCH TO ANOTHER BRANCH REMOTELY, EG: git push -f origin feature/something-cool:dev1"
alias delete_remote_branch="echo TO DELETE REMOTE: git push origin --delete branchname; git branch -d branchname"
alias shipit="git pull && git push"
alias master="git co master"
alias gmm="git merge master --no-ff"
gdesc() {
  git rev-parse --abbrev-ref HEAD | grep release > /dev/null 2>&1
  if [ $? == 0 ]; then
    ver=`git rev-parse --abbrev-ref HEAD | cut -d - -f 2`
    git describe --tags --abbrev=7 --match $ver
  else
    git describe --tags --abbrev=7
  fi
}
alias gdesc2="git describe --tags --abbrev=7 --always"
alias vigitconfig='vim `git rev-parse --show-toplevel`/.git/config'
alias wipush='git commit -m "wip"; git push'

alias glog="git log"
alias glogs="git log --stat"
alias glogp="git log -p"
alias guh="git log origin/master..HEAD"
alias gdif="git diff"
alias gdiff="git diff"
alias giton="git checkout"
alias gfal="git fetch --all"

ctrl_g_list_branches() {
  git branch -a #| sed 's/remotes\/origin\///'
}
ctrl_g_checkout() {
  branch=$1
  if [ $(echo $branch | grep '/') ]; then
    git checkout --track $branch
  else
    git checkout $branch
  fi
}
# bind '"":"git checkout --track $(checkoutablebranches | fzf)\n"'
bind '"":"ctrl_g_checkout $(ctrl_g_list_branches | fzf)\n"'
bind '"":"vim $(fzf)\n"'
bind '"":"cd $(find . -type d | fzf)\n"'

alias gbr="git branch -a"
alias gbrf="git branch -a | grep"
alias antlers="git branch -a"
alias antler="git branch -a | grep"

alias grb="gbr"
alias gimme="git pull"
alias gp="git pull"
alias gpp="git pull && git push"
alias shove="git push -u origin"
alias lshove="git push -u lama"
alias grab="git clone"
ggrab() {
  git clone https://github.com/$1.git
}
alias whorules="git shortlog -ns"
alias catg="cat .git/config"


# Dave rake


# Searching
alias psa='ps aux | grep program_agent'
alias psr='ps aux | grep ruby | grep -v grep'
alias pfind="ps auxww | grep -v grep | grep"
alias finds='find . | grep -v svn'
alias rfind='find . -name "*.rb" | xargs grep '
alias jfind='find . -name "*.java" | xargs grep '


#alias cbr="git co STORAGE-2412"
# alias cbr="git co turnstyle-batch-add-job"
alias cbr="git co STORAGE-2434"

alias vf='vim `fzf`'
alias chmox='chmod +x'
alias chmine='sudo chown -R $(whoami):$(groups|first)'

alias serve="python -m SimpleHTTPServer"

alias json="python -m json.tool"

alias sad="git ls-files --others --exclude-standard"
alias rmsad="sad | xargs rm -rf"


art() {
  cat ~/config/ascii-art/$1.ascii.txt
}

# jvim() {
#   fname=$(echo $1 | awk -F: '{print $1}')
#   lnum=$(echo $1 | awk -F: '{print $2}')
#   if [ -z "$lnum" ]; then
#     vim "$fname"
#   else
#     vim +$lnum "$fname"
#   fi
# }

findip() {
  ping -t 2 192.168.1.255 > /dev/null; 
  arp -a | grep -v incomplete | awk '/'$1'/ {print $2}' | sed 's/(//' | sed 's/)//'
}
findips() {
  arp -a |grep -v incomplete | awk '{print $2 " " $1 " (" $4 ")"}'
}


stubbashscript() {
  fname=$1
  echo '#!/bin/bash' > $fname
  echo 'pushd `dirname ${BASH_SOURCE[0]}` > /dev/null; HERE=`pwd`; popd > /dev/null' >> $fname
  echo 'cd $HERE' >> $fname
  chmod +x $fname
}

newbash() {
  fname=$1
  if [ -f $fname ]; then
    echo "$fname already exists."
  else
    stubbashscript $fname
    vim $fname
  fi
}

tmpbash() {
  fname=TEMP_bash_script.sh
  stubbashscript $fname 
  vim $fname
  rm $fname
}

gtvim() {
  fname=TEMP_git_status_script.sh
  stubbashscript $fname
  git status >> $fname
  vim $fname
  rm $fname
  git status
}


newpy() {
  fname=$1
  if [ -f $fname ]; then
    echo "$fname already exists."
  else
    echo '#!/usr/bin/env python' > $fname
    echo 'import os' >> $fname
    echo 'here = os.path.dirname(os.path.realpath(__file__))' >> $fname
    echo 'if __name__ == "__main__":' >> $fname
    echo '    print "Hello from '$fname', running in %s" % here' >> $fname
    chmod +x $fname
    vim $fname
  fi
}
newruby() {
  fname=$1
  if [ -f $fname ]; then
    echo "$fname already exists."
  else
    echo '#!/usr/bin/env ruby' > $fname
    echo '$LOAD_PATH.unshift(File.expand_path(File.dirname(__FILE__)))' >> $fname
    echo 'puts "Hello!"' >> $fname
    chmod +x $fname
    vim $fname
  fi
}

vw() {
  vim `which $1`
}

ver() {
  apt-cache search $1
  apt-cache policy $1 | head -n 50
}

pathmunge () {
        if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
           if [ "$2" = "after" ] ; then
              PATH=$PATH:$1
           else
              PATH=$1:$PATH
           fi
        fi
}

alias varrunlama="sudo mkdir -m 777 -p /var/run/lama"

upload-ssh-key() {
  host=$1
  if [ -z "$host" ]; then
    echo "What host?"
    exit 1
  fi
  ssh $host 'mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys' < ~/.ssh/id_rsa.pub
}

desc2sha() {
  #git show `echo "$1" | sed 's/^.*-g//'`
  git log --pretty=format:'%H' $1 | head -n 1
}

sha2desc() {
  git describe --tags --abbrev=7 --always $1

}

sha2branch() {
  echo `git describe --tags --abbrev=7 --always $1` | sed 's/\([0-9]\.[0-9]\.[0-9]\).*/release-\1/g'
}

desc2branch() {
  echo $1 | sed 's/\([0-9]\.[0-9]\.[0-9]\).*/release-\1/g'
}

gits() {
  desc2branch $1
  desc2sha $1
}


pkgurl() {
  sudo apt-get install -qq --reinstall --print-uris $1 | perl -lne "/'(.*?)'/;print \$1"
}


alias wdlg='wdl | grep'  # wd list grep
alias wdlf=wdlg
