#!/bin/bash

source_config "bash/before.sh"


source_config "bash/env.sh"

if [ -f "$HOME/prompt.sh" ]; then
  source "$HOME/prompt.sh"
else
  source_config "bash/prompt.sh"
fi

source_config "bash/wd.sh"
source_config "bash/aliases.sh"
source_config "bash/docker.sh"

source_config "bash/fzf.sh"

if [ -d ~/.gopath ]; then
  source_config "bash/gopath.sh"
fi

source_config "bash/googlecloud.sh"
source_config "bash/rust.sh"
source_config "bash/terminalbg.sh"

source_config "bash/after.sh"
