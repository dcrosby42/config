
alias lexec="lxc exec"
alias lsx="lxc list"
alias lsxi="lxc image list"

lbash() {
  lxc exec $1 -- /bin/bash
}

# newcontainer() {
#   if [ "$1" == "" ]; then
#     echo "Usage: newcontainer <image> <container> <ipaddr> [net=testbr0] [dev=eth0]"
#     return 1
#   fi
#   image=$1
#   con=$2
#   addr=$3
#   net=$4
#   if [ "$net" == "" ]; then
#     net=lamabr0
#   fi
#   dev=$5
#   if [ "$dev" == "" ]; then
#     dev=eth0
#   fi
#   set -x
#   lxc init $image $con
#   lxc network attach $net $con $dev
#   lxc config device set $con $dev ipv4.address $addr
#   lxc start $con
#   set +x
# }
