terminalbgfile=$HOME/.terminalbg
if [ -f $terminalbgfile ]; then
  TERMINALBG=`cat $terminalbgfile`
else
  TERMINALBG=DARK
fi

terminalbg() {
  TERMINALBG=$1
  echo "$TERMINALBG" > $HOME/.terminalbg
  export TERMINALBG
}

lightterm() {
  terminalbg LIGHT
}
darkterm() {
  terminalbg DARK
}

export TERMINALBG
