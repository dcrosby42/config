[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export FZF_TMUX=0  # split-panes don't autofocus properly 2016-01-25 crosby
export FZF_DEFAULT_COMMAND='pt --smart-case -l ""'
export FZF_CTRL_T_COMMAND='pt --smart-case -l ""'
