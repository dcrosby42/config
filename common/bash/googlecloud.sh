# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/crosby/googlecloud/download/google-cloud-sdk/path.bash.inc' ]; then source '/Users/crosby/googlecloud/download/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/crosby/googlecloud/download/google-cloud-sdk/completion.bash.inc' ]; then source '/Users/crosby/googlecloud/download/google-cloud-sdk/completion.bash.inc'; fi
