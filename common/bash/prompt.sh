#!/bin/bash
source_config "bash/git_prompt_functions.sh"

export PS1='\[\e[01;34m\]\u@\h:\[\e[01;33m\]\w \[\033[01;34m\]`parse_git_branch` \[\033[31m\]\n\[\e[01;33m\]] \[\e[00m\]'
