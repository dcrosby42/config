#!/bin/bash

dockerenv() {
  if [ -z "$1" ]; then
    docker-machine ls
  else
    eval "$(docker-machine env $1)"
  fi
  echo "Current Docker env vars:"
  env | grep DOCKER
}

dockerip() {
  docker-machine ip $(docker-machine active)
}

dockerbash() {
  docker exec -it $1 bash
}
