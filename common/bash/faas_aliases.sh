#!/bin/bash

alias vifaasalias="vim $MYCONFIG_COMMON/bash/faas_aliases.sh && source $MYCONFIG_COMMON/bash/faas_aliases.sh"

alias fit='egrep -rn "FIt\(|FContext\(|FDescribe\(|ShIt\(" * 2>&1 | grep -v "Binary\|while I watch the log\|onsi/ginkgo"'

alias psqemu="ps aux | grep `whoami`.*qemu | grep -v grep"
alias psf="ps aux | grep firecracker | grep start-time"
alias psfc="psf | wc -l"

alias pshtagent="ps aux | grep ht-agent | grep -v grep"

fnuke() {
  username=`whoami`
  for pid in `ps aux | grep "${username}.*qemu" | grep -v grep | awk '{print $2}'`; do
    sudo kill -9 $pid
  done
  sudo rm -f /tmp/vm-*.log
}

grelcheck() {
  b1=$1
  b2=$2
  re='^[0-9]+$'
  if [[ "$b1" =~ $re ]] ; then
    b1=release-1.${1}.0
  fi
  if [[ "$b2" =~ $re ]] ; then
    b2=release-1.${2}.0
  fi
  echo "Commits that are in $b1 but are NOT in $b2:"
  # echo "Press ENTER to run: git log $b1 ^$b2"
  # read
  echo "git log --pretty=oneline $b1 ^$b2"
  git log --pretty=oneline $b1 ^$b2
}
gpulls() {
  re='^[0-9]+$'
  for branch in "$@"; do
    if [[ "$branch" =~ $re ]] ; then
      branch=release-1.${branch}.0
    fi
    set -x
    git checkout $branch
    git pull
    set +x
  done
}

alias gksys="wd1; make ready; wd5; ginkgo"
