#!/bin/bash

wddir="$HOME/.wd"
if [ -d "${wddir}" ]; then
  export WDHOME=$wddir
  export WDSIZE=20
  source $WDHOME/wdaliases.sh
fi
