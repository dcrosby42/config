#!/bin/bash

alias vivagrantalias="vim $MYCONFIG_COMMON/bash/vagrant_aliases.sh && source $MYCONFIG_COMMON/bash/vagrant_aliases.sh"

alias vdestroy='vagrant destroy -f'
alias vdown='vagrant halt'
alias vhalt='vagrant halt'
alias vms='echo Running VMs:; VBoxManage list runningvms; echo; echo All VMs:; VBoxManage list vms'
alias vmsr='echo Running VMs:; VBoxManage list runningvms'
alias vnuke='vagrant destroy -f'
alias vpause='vagrant suspend'
alias vreload='vagrant reload'
alias vresume='vagrant resume'
alias vssh='vagrant ssh'
alias vstat='vagrant status'
alias vsuspend='vagrant suspend'
alias vup='vagrant up'


alias vash='ssh -F ssh_config'

alias vsnap='VBoxManage snapshot'

vbounce() {
  vagrant halt $1; vagrant up $1 
}

vboxsuspend() { 
  VBoxManage controlvm $1 savestate 
}

vboxdown() { 
  VBoxManage controlvm $1 acpipowerbutton 
}

vboxdownhard() { 
  VBoxManage controlvm $1 poweroff 
}
vboxdelete() { 
  VBoxManage unregistervm --delete $1 
}

vmnames() {
  VBoxManage list vms | grep $1 | cut -d'"' -f 2
}

double_roundhouse() {
  name=$1
  vmnames=$(VBoxManage list runningvms | grep $name | cut -d'"' -f 2)
  for x in ; do vboxdownhard $x; done
  vagrant up $name
  vagrant ssh $name
}

triple_roundhouse() {
  name=$1
  vms=$(VBoxManage list runningvms | grep $name | cut -d'"' -f 2)
  for vm in $vms; do vboxdownhard $vm; vboxdelete $vm; done
  vagrant up $name
  vagrant ssh $name
}

