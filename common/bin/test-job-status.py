#!/usr/bin/env python

# http://serverfault.com/questions/309848/how-to-check-the-build-status-of-a-jenkins-build-from-the-command-line
# author: ajs
# license: bsd
# copyright: re2

import os
import json 
import sys
import urllib
import urllib2
from datetime import datetime

here = os.path.dirname(os.path.realpath(__file__))

JenkinsUrl = "https://build.llnw.net/job/"

JobNames = [
    "go-lama-lmdapi-tests",
    "go-lama-lmdd-tests",
    "go-lama-brickd-tests",
    "lama-mapperd-tests",
    "lama-spapid-tests",
    "go-lama-ftp-proxy-tests",
    "lama-mass-ingest-tests",
]


def ms2date(msStr):
    ms = int(msStr)
    scale=3
    div = 10**scale
    dt = datetime.fromtimestamp(ms // div)
    # return dt
    s = dt.strftime("%Y-%m-%d %H:%M:%S")
    s += '.' + str(int(ms % div)).zfill(scale)
    return s


def get_job_status(job):
    urlstr = JenkinsUrl + job + "/lastBuild/api/json"
    print urlstr
    stream = urllib2.urlopen(urlstr)
    data = json.load(stream)
    return data

def shortform_status(data):
    name = '?name'
    msg = '?msg'
    commitId = '?commitId'
    changeSet = data.get("changeSet")
    if changeSet:
        items = changeSet.get('items')
        if items and len(items) > 0:
            item = items[0]
            name = item['author']['fullName']
            msg = item['msg']
            commitId = item['commitId']
    return "%s - %s @ %s - %s - %s - %s" % (data['fullDisplayName'], data['result'], ms2date(data['timestamp']), name, msg, commitId)

def summarize_many(job_names):
    for job in job_names:
        data = get_job_status(job)
        print shortform_status(data)

summarize_many(JobNames)
# print get_job_status("go-lama-lmdapi-tests")
# print shortform_status(get_job_status("go-lama-lmdapi-tests"))

