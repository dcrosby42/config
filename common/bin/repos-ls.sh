#!/bin/bash
pushd `dirname $0` > /dev/null; HERE=`pwd`; popd > /dev/null

month=$1
if [ -z "$month" ]; then
  month=`date +%Y-%m`
fi

xenial_repo_url="http://repo.llnw.net/lama/dev/ubuntu/xenial/binary"
precise_repo_url="http://repo.llnw.net/lama/dev/ubuntu/precise/binary"

# url=${repo_url}/${month}/
for repo in $xenial_repo_url $precise_repo_url; do
  url=${repo}/${month}/
  for deb in `curl -s $url | grep -oP '(?<=href=")[^"]*(?=")' | grep ".deb$"`; do
    echo ${url}${deb}
  done
done
