#!/bin/bash
pushd `dirname $0` > /dev/null; HERE=`pwd`; popd > /dev/null

month=$1
if [ -z "$month" ]; then
  month=`date +%Y-%m`
fi

repo_url="http://repo.llnw.net/lama/dev/ubuntu/xenial/binary"
url=${repo_url}/${month}/


for deb in `curl -s $url | grep -oP '(?<=href=")[^"]*(?=")' | grep ".deb$"`; do
  echo ${url}${deb}
done
