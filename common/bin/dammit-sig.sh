#!/bin/bash

for srcfile in `pt --nocolor --nogroup "\"git.llnw.com/lama/go-lmd/"  | cut -d: -f1 | sort | uniq`;  do 
  echo Fixing: $srcfile ...
  perl -i -p -e 's/\/go-lmd\//\/go-lmd.git\//' $srcfile
done

for srcfile in `pt --nocolor --nogroup "\"git.llnw.com/lama/go-protoutil/"  | cut -d: -f1 | sort | uniq`;  do 
  echo Fixing: $srcfile ...
  perl -i -p -e 's/\/go-protoutil\//\/go-protoutil.git\//' $srcfile
done
