#!/bin/bash

prefix=http://global.mt.lldns.net/llnwstor/secure/packages/dev/ubuntu/xenial/binaries
manurl=$prefix/manifest.txt

match=$1
if [ -z "$match" ]; then
  (>&2 echo $manurl)
  curl -s $manurl
else
  hits=`curl -s $manurl | grep $match | sed 's/^\.\///' | sort`
  for hit in $hits; do
    echo $prefix/$hit
  done
fi
