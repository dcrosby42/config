#!/bin/bash

# Generate a github url to the current file/line 

# In vim:
# nnoremap <leader>gl :execu:te ":!glink.sh" bufname('%') line('.')<CR>
#

git_branch=`git rev-parse --abbrev-ref HEAD`
git branch -vv | grep "* ${git_branch}" | awk '{print $4}' | sed 's/\[//' | sed 's/\/.*//' | sed 's/\]//'
