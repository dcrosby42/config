#!/bin/bash
# pushd `dirname $0` > /dev/null; HERE=`pwd`; popd > /dev/null

# Any redis server:
# ps_name="/usr/bin/redis-server"

# For our redis test driver in go-lmd land:
# ps_name="/usr/bin/redis-server -"

# watch -n1 "sudo lsof -p $(pgrep tcpproxy)| grep TCP | grep -v LISTEN"

ps_name="$1"

dash_el=""
if [ "$2" == "-l" ]; then
  dash_el="1"
fi


while true; do
  the_pid=`pgrep "$ps_name"`
  while [ -z "$the_pid" ]; do
    echo "Waiting for process to appear: $ps_name"
    sleep 1
    the_pid=`pgrep "$ps_name"`
  done
  echo "====================================================================================" | tee -a $log
  echo Process "$ps_name" pid=$the_pid
  if [ "$dash_el" == "1" ]; then
    sudo lsof -p $the_pid | grep TCP
  else
    sudo lsof -p $the_pid | grep TCP | grep -v LISTEN
  fi
  sleep 1
done
