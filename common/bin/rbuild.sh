#!/bin/bash

set -e
for n in `ls`; do
  if [ -d $n ]; then
    echo Building $n
    pushd $n >/dev/null
    go build -i ./...
    popd > /dev/null
  fi
done
