#!/usr/bin/ruby
class Estimator
	def usage(err_msg=nil)
		puts err_msg if err_msg
		puts "Usage: estimator.rb 500..1000 Website eCommerce Legacy_Integration"
		exit 1
	end

	def run
		raise "Need range and categories" unless ARGV.size > 1
		range = eval(ARGV.shift)#500..1500
		cats = ARGV.dup # [ 'Web site', 'Database', 'eCommerce' ]

		base = (range.first + rand * (range.last-range.first)).to_i
		
		items = {}
		pool = base
		cats.each_with_index do |cat,i|
			amt = -1
			if i == (cats.size - 1)
				amt = pool
				pool = 0
			else
				half = pool / 2.0
				adjust = rand(0.3 * half)
				amt = half - rand(0.5 * half) 
				pool -= amt
			end
			items[cat] = amt
		end

		items.each do |k,v|
			puts "#{k}: #{rough(v)}"
		end

		mgmt = (base * 0.1).to_i
		total = base + mgmt
		puts "Base Hours: #{rough(base)}"
		puts "Project Management: #{rough(mgmt)}"

		puts "Total ~ #{rough(total)} man hours"
		puts "      ~ #{rough(total/2.0)} pair hours"
		puts "      ~ #{total/2.0/80} pair weeks"

		rate = 125
		puts "At $#{rate}/hr: $#{rough(total) * rate}"
	end


	protected
	def rough(num, acc=10)
		(num.to_i / acc) * acc
	end

	def here; File.expand_path(File.dirname(__FILE__)); end

	def valid_file(fname)
		raise "Can't find file #{fname}" unless File.exists?(fname)
		fname
	end

	def valid_dir(dirname)
		raise "Can't find dir #{dirname}" unless File.exists?(dirname)
		dirname
	end
	def search_file_and_replace(file_name, pattern, replacement)
		text = File.read(valid_file(file_name))
		text.gsub!(pattern, replacement)
		File.open(file_name, "w") do |f| f.write text end
	end

	def create_file_from_template(dest_file, template_file)
		raise "#{dest_file} already exists; remove it first" if File.exists?(dest_file)

		template_file = valid_file("#{here}/rscript_template.rb")
		template = ERB.new(File.read(template_file))

		File.open(dest_file,"w") do |f|
			f.write template.result(binding)
		end
	end

  def humanize(lower_case_and_underscored_word)
    lower_case_and_underscored_word.to_s.gsub(/_id$/, "").gsub(/_/, " ").capitalize
  end

  def camelize(lower_case_and_underscored_word)
    lower_case_and_underscored_word.to_s.gsub(/\/(.?)/) { "::" + $1.upcase }.gsub(/(^|_)(.)/) { $2.upcase }
  end

  def titleize(word)
    humanize(underscore(word)).gsub(/\b([a-z])/) { $1.capitalize }
  end
  
  def underscore(camel_cased_word)
    camel_cased_word.to_s.gsub(/::/, '/').gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').gsub(/([a-z\d])([A-Z])/,'\1_\2').downcase
  end
end

if $0 == __FILE__
	script = Estimator.new
	begin
		script.run
	rescue Exception => e
		script.usage e.message
	end
end
