#!/usr/bin/ruby
require 'webrick'

port = ARGV.shift
port = port.to_i if port
port ||= 2400 

s = WEBrick::HTTPServer.new :Port => port, :DocumentRoot => Dir.pwd 
trap(:INT) { s.shutdown }
s.start
