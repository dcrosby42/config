#!/bin/sh
if [ -z "$1" ]; then
	echo "Need hostname"
else
	scp ~/.ssh/id_rsa.pub $1:.ssh/stargate_rsa.pub
fi
