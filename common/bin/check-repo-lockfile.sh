#!/bin/bash
pushd `dirname ${BASH_SOURCE[0]}` > /dev/null; HERE=`pwd`; popd > /dev/null
cd $HERE

lockfile=/opt/repo/lama/dev/ubuntu/precise/.git/index.lock

output=$(ssh repo "stat $lockfile" 2>/dev/null)
if [ "$?" == "0" ]; then
  # stat hit something
  echo "== LOCK FILE EXISTS =="
  echo $lockfile
  echo $output
else
  echo "== Lock file not present.  All ok. =="
fi

ssh repo "ls -l /opt/repo/lama/dev/ubuntu/precise/binary | grep Packages" 2>/dev/null
