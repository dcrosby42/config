#!/bin/bash
pushd `dirname ${BASH_SOURCE[0]}` > /dev/null; HERE=`pwd`; popd > /dev/null
cd $HERE

buildno=$1
if [ -z "$buildno" ]; then
  echo "Build number...?\nAborting"
  exit 1
fi

url="https://build.llnw.net/job/faas-ci-tests/node=faas-os-ubuntu-1604/${buildno}/console"
file="/tmp/build_${buildno}.txt"
curl -f $url > $file
if [ $? != 0 ]; then
  curl -v $url
  echo "Badness."
  echo "NOT UPLOADING $file"
  rm -f $file
  exit 1
fi
echo Wrote $file

export TOKEN="3cc1bc6b-7115-4bd4-8ca8-0d712291fda3"
agile upload $file faas-dev/faas-ci-test-captures
echo "Uploaded $file to faas-dev/faas-ci-test-captures"
