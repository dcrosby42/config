#!/usr/bin/env node
// npm i --global 'cron-parser'
const cronParser = require('cron-parser');

let [expr, nStr] = process.argv.slice(2)

if (!expr) {
  expr = "* * * * * *"
}

const interval = cronParser.parseExpression(expr)

let n = 5
if (nStr) {
  n = parseInt(nStr)
}
console.log(`Expression: '${expr}'`)
console.log(`Next ${n} timings:`)
for (let i = 0; i < n; i++) {
  console.log('Date: ', interval.next().toString());
}
