#!/bin/bash

match=$1

url1=http://global.mt.lldns.net/llnwstor/faas/vms/dev/index.txt
url2=http://global.mt.lldns.net/llnwstor/faas/vms/prod/index.txt

if [ -z "$match" ]; then
  echo $url1
  curl -s $url1
  echo $url2
  curl -s $url2
else
  echo $url1
  curl -s $url1 | grep $match | sort
  echo $url2
  curl -s $url2 | grep $match | sort
fi
