#!/bin/bash
# pushd `dirname $0` > /dev/null; HERE=`pwd`; popd > /dev/null

# Any redis server:
# ps_name="/usr/bin/redis-server"

ps_name="/usr/bin/redis-server -"  # the redis started by our test harness
# ps_name="/usr/bin/redis-server"  # system redis server

while true; do
  the_pid=`pgrep -f "$ps_name"`
  while [ -z "$the_pid" ]; do
    echo "Waiting for process to appear: $ps_name"
    sleep 1
    the_pid=`pgrep -f "$ps_name"`
  done
  echo Process "$ps_name" pid=$the_pid

  redis_port=$(sudo lsof -p $the_pid | grep TCP | grep LISTEN | awk '{print $9}' | awk -F: '{print $2}')
  echo "redis_port=$redis_port"
  while [ -z "$redis_port" ]; do
    echo "Waiting for pid=$the_pid to bind server port..."
    sleep 1
    redis_port=$(sudo lsof -p $the_pid | grep TCP | grep LISTEN | awk '{print $9}' | awk -F: '{print $2}')
  done

  redis-cli -p $redis_port
done
