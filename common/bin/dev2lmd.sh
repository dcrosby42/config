#!/bin/bash
# pushd `dirname ${BASH_SOURCE[0]}` > /dev/null; HERE=`pwd`; popd > /dev/null
# cd $HERE

s=$GOPATH/bin/lmdapid
d=/usr/bin/lmdapid-dev

set -e 
set -x 

sudo rm -f $s $d /tmp/path_prefix*
go install git.llnw.com/lama/go-lmd.git/apps/lmdapi/lmdapid
sudo cp $s $d
sudo chown root:root $d
sudo chmod 755 $d

sudo sv stop lmdapi-s1
sudo sv start lmdapi-s1
