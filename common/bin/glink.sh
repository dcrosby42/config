#!/bin/bash

# Generate a github url to the current file/line 

# In vim:
# nnoremap <leader>gl :execu:te ":!glink.sh" bufname('%') line('.')<CR>
#

fname=$1
lnum=$2
if [ -z "$fname" ]; then
  echo "Please supply name of file."
  exit 1
fi
fullname=`git ls-files --full-name $fname`
if [ -z "$fullname" ]; then
  echo "Failed: git ls-files $fname"
  exit 1
fi

git_branch=`git rev-parse --abbrev-ref HEAD`
echo "git_branch=$git_branch"

current_remote=`git branch -vv | grep "* ${git_branch}" | awk '{print $4}' | sed 's/\[//' | sed 's/\/.*//' | sed 's/\]//'`
echo "current_remote=$current_remote"

base='https://'`git config --get remote.${current_remote}.url | sed 's/git@//' | sed 's/\.git$//' | sed 's/:/\//'`

url="${base}/blob/${git_branch}/${fullname}"

if [ ! -z "$lnum" ]; then
  url="${url}#L${lnum}"
fi

echo $url
