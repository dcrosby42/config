#!/usr/bin/ruby

class NameSource
  def initialize
    @actual_names = []
    @first_names = []
    @last_names = []
    DATA.readlines.each do |line|
      parts = line.strip.split(/ /)
      first,last = parts
      if parts.size > 2
        last << " " << parts[2..-1].join(" ")
      end
      @actual_names << [first,last]
      @first_names << first
      @last_names << last
    end
  end
  
  def get_some_names(num=10)
    arr = []
    s = @actual_names.size
    num.times do 
      actual = @actual_names[rand(s)]
      arr << actual.join(" ")
    end
    arr
  end

  def shuffled_names(num=10)
    arr = []
    fs = @first_names.size
    ls = @last_names.size
    num.times do 
      first = @first_names[rand(fs)]
      last = @last_names[rand(ls)]
      arr << "#{first} #{last}"
    end
    arr
  end
end

name_source = NameSource.new
#puts name_source.get_some_names
puts name_source.shuffled_names(100)


__END__
Hugh Sykes
Rebecca Newman
Lucinda Moore
Kate Ferrand
James Read
Maxine Mawhinney
Andrew North
Gary O’Donoghue
Greg Wood
Kevin Bishop
Bridget Kendall
Matt Barbet
Guy Pelham
Ming Tsang
Dan Kelly
Mandy Baker
Dougie Dalgleish
Sean Klein
Lorna Donlon
Robert Chaundy
Fiona Anderson
Lucy Wilkins
Lesley Curwen
Khalid Javed
Rowan Bridge
Jonathan Whitney
Chris Eakin
Alan Reed
Ben Thomas
Abigail Simmons
Piers Parry-Crooke
Sanchia Berg
John Curran
Christine McGourty
Elaine Lester
Mark Sanders
Arif Ansari
Gill Pulsford
Mark D’Arcy
Laura Trevelyan
Geraint Owen
Alison Macdonald
Ken Wilson
Sarah Parrish
Rachel Horne
Mike Embley
Jane Ashley
James Hardy
Claire Hughes
Jon Devitt
Daniel Mann
Tim Allman
Nick Edser
Judith Burns
James Menendez
Joanna Carr
Nicola Kohn
Adam Porter
Frances Lass
Dominic Hughes
Mark Lobel
Monica Soriano
Lucy Crystal
Ben Bevington
Wietske Burema
Jon Brain
Hanna White
Catherine Miller
Jon Cronin
Ben Ando
John Shields
Nicholas Wallis
Jo Parsons
Andy Lewis
Tim Franks
Ian Pannell
Eleanor Moritz
Kenneth Payne
Manjushri Mitra
Mike Costello
Andrew Hosken
David Crawford
Adam Brimelow
Gail Ashton
Jonathan Aspinwall
Jenny Baxter
Jonathan Paterson
Richard Colebourn
Sue Oates
Katie Finigan
Louise Cotton
Georgina Pattinson
Mike Sergeant
Neil Strickland
Ben Brown
Lizzi Watson
Malcolm Senior
Mick Robson
Olivia Macleod
Madeleine Morris
Richard Jarrett
Elliot Choueka
Andy Tighe
Sue Emmett
Robyn Hunter
David Hannah
Lucy Rodgers
Tim Whewell
Patrick Jackson
Jane Corbin
Parv Ramchurn
Tom Giles
Cathie Mahoney
Sally Heptonstall
Caroline Alton
Gaetan Portal
Poonam Taneja
Alison Mann
Patricia Daganskaia
Jasmin Buttar
Andrew Walker
Carolyn Rice
Peter Machin
Angie Nehring
Daniel Boettcher
Fiona Blair
Nicola Dann
Lesley Taylor
Anita Coulson
Krista Beighton
Alex Millar
Zillah Watson
Robert Corp
Terry Dignan
Helen Wade
Finlo Nelson Rohrer
Kate Goldberg
Ben Fell
Simon Bendle
Neil Heathcote
Johanna Howitt
Shelley Charlesworth
Tim Samuels
Dan Simmons
Gillian Hargreaves
Geoff Stayton
Matthew Leach
David Shukman
Gerry Kiernan
Ros Tamblyn
Robert Greenall
Iain Carter
Margaret Skinner
Richard Watson
Nathalie Knowles
Norman Smith
Amber Dawson
Angus Crawford
Paul Jones
Alicia McCarthy
John Beesley
Ian Lauchlan
Emma Jane Kirby
Darryl Chamberlain
Tim Haynes
Lucy Barrick
Liz Shaw
Ray Alexander
Huw Edwards
Rob Shepherd
Stephanie Power
Karen Wightman
Pete Jump
Philippa Busby
Gordon Farquhar
Sarah Teasdale
Sarah Tempest
Melanie Parry
Michael O’Connor
Richard Danbury
Gary Eason
Clive Myrie
Michele Grant
Philip Palmer
Andrew Fletcher
Jonathan Chapman
Helena Wilkinson
James Robbins
Allan Little
Pallab Ghosh
Susan Hulme
Jonny Dymond
Gordon Corera
Nora Dennehy
Karen Hoggan
Jonty Bloom
Joanne Hilditch
Jo Coburn
Branwen Jeffreys
Rebecca Marston
Caroline Bayley
Michael Voss
James Reynolds
Jo Floto
MikeWooldridge
Robin Chrystal
Anthony Massey
Rachel Hooper
Reeta Chakrabarti
Nick Higham
Gillian Lacey-Solymar
Rory Maclean
Carolyn Quinn
Will Walden
Vikki Clein
David Thompson
Carole Walker
Hugh Pym
Oggy Boytchev
Adele Tobe
Jonathan Marcus
Jonathan Charles
Doug Dalgleish
Rob Watson
Andrew Burroughs
Deborah Dwek
Danny Shaw
Judith Moritz
Jenny Culshaw
Michele Grant
Stephen Evans
Tom Symonds
Jane Peel
Candida Watson
Jane Dreaper
Paul Danahar
Nils Blythe
Nicholas Blakemore
Ian Jolly
Caroline Alton
Pauline McCole
Rory Cellan-Jones
Martin Shankleman
Catherine Norris-Trent
Magnus McGrandle
John Moylan
Philippa Goodrich
Tom Geoghegan
Alison Francis
Martin Fookes
Mandy Stokes
Jennifer Sneesby
Nigel Pankhurst
Katya Adler
Andrew Webb
Paul Waters
Samantha Smith
Jo-Anne Pugh
Paul Kirby
Adrian Dalingwater
Roxanne Panthaki
Tamsin Curnow
Keith Hindell
Martin Rosenbaum
Clare Csonka
Jane Hughes
Lisa Hampele
Ben Shore
Charlotte Smith
Claudia Allen
Anna Holford
Kate Ford
Tory Scott
Nick Tarry
Gareth Jones
Richard Pattinson
Phil Hendry
Sarah Deech
Dharshini David
Marianne Degen
Tom Butler
Tony Smith
Mark Mitchell
Ruth Cobbe
David Gibson
Oliver Del Mar
David Lennon
Caroline Donne
Jacey Normand
Laurence Inwood
Peter Trollope
Ann Wrights
Connie Pollard
Alex Gerlis
Nick Ravenscroft
Sue Polhill
Mary Gahan
Adam Cumiskey
Crispin Thorold
Jenny Matthews
Charlotte Nicol
Neil Churchman
Katy Hickman
John Sweeney
Natalie Lisbona
Ian Kenyon
Andrea Chipman
Colin Hazelden
Martin Morgan
Haoyu Zhang
Ian Rose
Kirsteen Knight
Kavitha Prasad
Polly Billington 
