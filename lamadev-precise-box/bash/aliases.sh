source_common_config "bash/aliases.sh"


alias tmc="script -qfc 'tmux -CC' /dev/null"
alias tma="tmux -CC attach"

alias serve="python -m SimpleHTTPServer"

alias wake_up_neo="cmatrix -bu 6"

source_common_config "bash/lmd_aliases.sh"

pkgver() {
  apt-cache policy $1 | grep "\*\*\*" | awk '{print $2}'
}

alias myinst1="mysql --table -u root -padmin123 lama_inst1"
alias myshared="mysql --table -u root -padmin123 lama_shared"
alias myturnstyle="mysql --table -u root -padmin123 turnstyle"
