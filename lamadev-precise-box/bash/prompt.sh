source_config "bash/git_prompt_functions.sh"

export WORKDAY_PS1='\[\e[38;5;190m\]lamadev-precise \[\e[38;5;136m\]\w \[\e[1;34m\]$(parse_git_branch)\[\e[0m\]\n\[\e[1;33m\]>\[\e[0m\] '

export C64_PS1='READY. \[\e[38;5;62m\]\w $(parse_git_branch)\[\e[0m\]\n'

export PS1=$WORKDAY_PS1
