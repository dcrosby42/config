#!/bin/bash

export LAMA_TEMP_ROOT=/tmp/dcrosby

#export SHELL=xterm-256color
export LC_ALL=en_US.utf-8
export LANG="$LC_ALL"

export BUILD_ENVIRONMENT="lamadev-precise-dcrosby"
# To use home-built go-ness
# export PATH=$HOME/src/go/bin:$PATH

# go-lmd-ws test suites:
export INCLUDE_SLOW_TESTS=1


#source_config "ruby/rbenv.sh"

source_common_config "perl/env.sh"

if [ -z "$(echo $PATH | grep sbin)" ]; then
  export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin
fi
export PATH=$PATH:$HOME/go/bin



source_common_config "bash/jeltz.sh"


