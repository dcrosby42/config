#!/bin/bash
pushd `dirname $0` > /dev/null; HERE=`pwd`; popd > /dev/null

url=$1
name=$2
fname=$HERE/download/$name

curl -s $url > $fname
scp $fname dcrosby@llp-03.dal.llnw.net:content/imgrelay/$name
