#!/bin/bash
 
user=dcrosby
host="home-office"
dir="~/config/tools"
git="STORAGE-1898"
for color in {0..256} ; do #Colors
  #Display the color
  echo -e "${color}: \033[38;5;${color}m${user}@${host} \033[1;33m${dir} \033[1;34m(${git})\033[0m"
done
echo #New line
 
exit 0

