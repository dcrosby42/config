#!/bin/bash
echo
echo FOREGROUNDS:
echo
echo -e "Default \033[39mDefault\033[0m"
echo -e "Default \033[30mBlack\033[0m"
echo -e "Default \033[31mRed\033[0m"
echo -e "Default \033[32mGreen\033[0m"
echo -e "Default \033[33mYellow\033[0m"
echo -e "Default \033[34mBlue\033[0m"
echo -e "Default \033[35mMagenta\033[0m"
echo -e "Default \033[36mCyan\033[0m"
echo -e "Default \033[37mLight gray\033[0m"
echo -e "Default \033[90mDark gray\033[0m"
echo -e "Default \033[91mLight red\033[0m"
echo -e "Default \033[92mLight green\033[0m"
echo -e "Default \033[93mLight yellow\033[0m"
echo -e "Default \033[94mLight blue\033[0m"
echo -e "Default \033[95mLight magenta\033[0m"
echo -e "Default \033[96mLight cyan\033[0m"
echo -e "Default \033[97mWhite\033[0m"

echo
echo BACKGROUNDS:
echo
echo -e "Default \033[49mDefault\033[0m"
echo -e "Default \033[40mBlack\033[0m"
echo -e "Default \033[41mRed\033[0m"
echo -e "Default \033[42mGreen\033[0m"
echo -e "Default \033[43mYellow\033[0m"
echo -e "Default \033[44mBlue\033[0m"
echo -e "Default \033[45mMagenta\033[0m"
echo -e "Default \033[46mCyan\033[0m"
echo -e "Default \033[47mLight gray\033[0m"
echo -e "Default \033[100mDark gray\033[0m"
echo -e "Default \033[101mLight red\033[0m"
echo -e "Default \033[102mLight green\033[0m"
echo -e "Default \033[103mLight yellow\033[0m"
echo -e "Default \033[104mLight blue\033[0m"
echo -e "Default \033[105mLight magenta\033[0m"
