#!/bin/bash

export MYCONFIG=~/config
source "$MYCONFIG/util/setup.sh"

if [ "$1" == "-f" ]; then
  force_config_files=1
  echo "Using -f to update existing symlinks."
fi

set_src_path() {
  given_path=$1
  src_path=""

  # Use box-specific config if present:
  my_conf_path="$MYCONFIG_BOX/$given_path"
  if [ -e "$my_conf_path" ]; then
    src_path="$my_conf_path" # SIDE EFFECT: $src_path MAY NOW BE USED BY OTHER CODE
  else
    # If no box-specific config exists, look for common:
    common_conf_path="$MYCONFIG_COMMON/$given_path"
    if [ -e "$common_conf_path" ]; then
      src_path="$common_conf_path" # SIDE EFFECT: $src_path MAY NOW BE USED BY OTHER CODE
    else
      echo "symlink_config_file: Cannot find '$my_conf_path' or '$common_conf_path'"
      return
    fi
  fi
}

symlink_config_file() {
  set_src_path $1
  if [ -z "$src_path" ]; then return; fi
  dest_path=$2


  # Check to see if the target already exists:
  debug_myconfig "WANT symlink $dest_path -> $src_path"
  existing_file=""
  existing_dir=""
  existing_link=""
  [ -f "$dest_path" ] && existing_file="1"
  [ -d "$dest_path" ] && existing_dir="1"
  [ -L "$dest_path" ] && existing_link="1"
  
  if [ -z "${existing_file}${existing_dir}${existing_link}" ]; then
    # Create a new symlink
    echo "Creating symlink $dest_path -> $src_path"
    ln -s "$src_path" "$dest_path"
  else
    if [ -z "$force_config_files" ]; then
      # File exists but we're not forcing overwrites
      echo "Already exists: $dest_path"
      return
    else
      if [ -L "$dest_path"  ]; then
        # Overwrite the symlink
        echo "Updating symlink $dest_path -> $src_path"
        rm "$dest_path"
        ln -s "$src_path" "$dest_path"
      else
        if [ -d "$dest_path" ]; then
          # ...er, there's an actual directory in the way. Bail.
          echo "Cowardly refusing to delete dir $dest_path.  Remove it yourself."
          return
        else
          echo "Cowardly refusing to delete file $dest_path.  Remove it yourself."
          return
        fi
      fi
      return
    fi
  fi
}

# Bash
symlink_config_file bash/dot_profile ~/.profile
symlink_config_file bash/dot_bashrc ~/.bashrc
symlink_config_file bash/dot_bash_logout ~/.bash_logout

# Git
#symlink_config_file git/dot_gitconfig ~/.gitconfig  # dot_gitconfig uses git config --global push.default matching
symlink_config_file git/dot_gitconfig2 ~/.gitconfig  # dot_gitconfig2 uses git config --global push.default simple
symlink_config_file git/dot_gitignore ~/.gitignore

# Vim
symlink_config_file vim ~/.vim
ln -sf ~/.vim/main.vimrc ~/.vimrc
#symlink_config_file NERDTree/dot_NERDTreeBookmarks ~/.NERDTreeBookmarks

# TMUX
symlink_config_file tmux/dot_tmux.conf ~/.tmux.conf

# Personal runnables:
symlink_config_file bin ~/bin
