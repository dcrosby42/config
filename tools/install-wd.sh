#!/bin/bash

MYCONFIG=~/config

wddir=~/.wd
if [ ! -f "$wddir" ]; then
  cp -r "$MYCONFIG/tools/wd" "$wddir"
else
  echo "Already exists, not installing: $wddir"
fi
