#!/bin/bash

MYCONFIG=~/config

fzfdir=~/.fzf
if [ ! -f "$fzfdir" ]; then
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && ~/.fzf/install
else
  echo "Already exists, not installing: $fzfdir"
fi

echo 
echo FZF ok.
echo
echo "Consider appending this to your .vimrc:"
echo "set rtp+=~/.fzf"
echo "map <leader>t :FZF!<CR>"
