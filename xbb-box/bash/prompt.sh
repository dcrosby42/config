source_config "bash/git_prompt_functions.sh"

vpn_indicator() {
  conn=$(scutil --nc list | grep Connected)
  if [ ! -z "$conn" ]; then
    echo "vpn"
  fi
}
user_color='\[\033[38;5;38m\]'
dir_color='\[\033[0;33m\]'
git_color='\[\033[1;34m\]'
# vpn_color='\033[38;5;129m'
vpn_color='\[\033[97;44m\]'
# vpn_color='\033[97m'
pr_color='\[\033[1;33m\]'
reset='\[\033[0m\]\]'
export PS1=$user_color'\u@\h '$dir_color'\w '$git_color'$(parse_git_branch) '$vpn_color'$(vpn_indicator)'$reset'\n'$pr_color'$\[\033[0m\] '

# export PS1='\[\e[01;36m\]\u@\h:\[\e[01;33m\]\w \[\033[01;34m\]$(parse_git_branch) \[\033[31m\]\n\[\e[01;33m\]] \[\e[00m\]'
