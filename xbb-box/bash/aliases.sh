source_common_config "bash/aliases.sh"
source_common_config "bash/lmd_aliases.sh"

alias jared="ssh jared"
alias ljared="ssh ljared"
alias garslo="ssh garslo"
alias mktags="ctags -R ."
alias crozdev1="ssh crozdev1"

alias love="/Applications/love.app/Contents/MacOS/love"
alias love10="/Applications/love_10.app/Contents/MacOS/love"


source_common_config "bash/vagrant_aliases.sh"

# alias con="ssh con"
alias con="cd ~/vagrant/containers && vagrant ssh"
alias ccon="cd ~/vagrant/containers && vagrant up && vagrant ssh"
alias ef="ssh edgefork"
alias edgefork="cd ~/vagrant/edgefork && vagrant up && vagrant ssh"
alias edgeforkup="cd ~/vagrant/edgefork && vagrant up && vagrant ssh"

alias snd.whistle="afplay ~/svn/crosby/drake/sounds/whistle.wav"
alias luke=snd.whistle
alias snd.klaxon="afplay ~/svn/crosby/drake/sounds/klaxon.wav"
alias snd.alert="afplay ~/svn/crosby/drake/sounds/alert.wav"

alias aa="cd ~/git/animal_house && rake"


alias jeltzme="wdscheme jeltz; wd; source env.sh"

alias dev1="ssh dev1"
alias faas1="ssh faas1"
alias faas10="ssh faas10"
alias lamadev="ssh lamadev"
alias lamadev1="ssh lamadev1"
alias lamadevp="ssh lamadevp"
