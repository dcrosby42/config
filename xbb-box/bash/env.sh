alias ls="ls -G"
source_common_config "bash/env.sh"

export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
export PATH=$HOME/gobin:$PATH
